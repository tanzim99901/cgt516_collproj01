﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowTipAtStartup : MonoBehaviour
{
	public GameObject RightTip;
	public GameObject LeftTip;
	public GameObject CameraTip;
	
	private bool TipShown = false;
    // Start is called before the first frame update
    void Start()
    {
		RightTip.SetActive(true);
		LeftTip.SetActive(true);
		CameraTip.SetActive(true);
		TipShown = true;
        //StartCoroutine(ShowHideTip());
    }

    IEnumerator ShowHideTip()
    {
        RightTip.SetActive(true);
		LeftTip.SetActive(true);
		CameraTip.SetActive(true);
		//TipShown = true;
        yield return new WaitForSeconds(5);
        RightTip.SetActive(false);
		LeftTip.SetActive(false);
		CameraTip.SetActive(false);
		//TipShown = false;
    }
	
	void Update()
	{
		
		/*if ((OVRInput.GetUp(OVRInput.RawButton.X) || OVRInput.GetUp(OVRInput.RawButton.A)) && TipShown)
		{
			RightTip.SetActive(false);
			LeftTip.SetActive(false);
			CameraTip.SetActive(false);
			TipShown = false;
		}
		
		if ((OVRInput.GetDown(OVRInput.RawButton.X) || OVRInput.GetDown(OVRInput.RawButton.A)) && !TipShown)
		{
			RightTip.SetActive(true);
			LeftTip.SetActive(true);
			CameraTip.SetActive(true);
			TipShown = true;
		}*/
		
		if ((OVRInput.GetUp(OVRInput.RawButton.X) || OVRInput.GetUp(OVRInput.RawButton.A)) && TipShown)
		{
			RightTip.SetActive(false);
			LeftTip.SetActive(false);
			CameraTip.SetActive(false);
			TipShown = false;
		}
		else if ((OVRInput.GetUp(OVRInput.RawButton.X) || OVRInput.GetUp(OVRInput.RawButton.A)) && !TipShown)
		{
			RightTip.SetActive(true);
			LeftTip.SetActive(true);
			CameraTip.SetActive(true);
			TipShown = true;
		}
	}
}
