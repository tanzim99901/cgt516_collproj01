﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisablePlayerCollider : MonoBehaviour
{
	public bool DisableCollider = false;
    // Start is called before the first frame update
    void Start()
    {
		Collider col = GetComponent<Collider>();
		if (DisableCollider)
		{
			col.enabled = false;
		}
		else if (!DisableCollider)
		{
			col.enabled = true;
		}
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
