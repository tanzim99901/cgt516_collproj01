﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Photon.Realtime;
using Photon.Pun;

namespace Com.CGT516.ApplePicker
{
	public class AppleController : MonoBehaviourPun
	{
	   
		//public OVRGrabbable grab01;
		//public GameObject thisApple;
		public OVRGrabber hand;
		public int GrabbableLayer;
		public int DefaultLayer;
		
		// Rigidbody variable to hold the player ball's rigidbody instance
		private Rigidbody rb;
		private OVRGrabbable grab01;
		private Renderer appleRenderer;
		private float channelOne, channelTwo, channelThree;
		private Color newAppleColor;
		private bool A_Button;
		private bool B_Button;
		private bool X_Button;
		private bool Y_Button;
		private float LTriggerButton;
		private float RTriggerButton;
		
		private int ColorNumber = 1;
		private bool IsColorChanged = false;
		
		Color AppleColor;

		// Called before the first frame update
		void Start()
		{
			// Assigns the player ball's rigidbody instance to the variable
			rb = GetComponent<Rigidbody>();
			appleRenderer = GetComponent<Renderer>();
			
		}

		// Called once per frame
		private void Update()
		{
			grab01 = GetComponent<OVRGrabbable>();
			hand = GetComponent<PunOVRGrabbable>().m_grabbedBy;
			
			if (grab01.isGrabbed)
			{
				gameObject.layer = GrabbableLayer;
			}
			else
			{
				gameObject.layer = DefaultLayer;
			}
			
			/*if (grab01.grabDone)
			{
				rb.useGravity = true;
				rb.isKinematic = false;
				//Debug.Log(rb.useGravity);
				//Debug.Log(rb.isKinematic);
				
			}
			else if (grab01.isGrabbed)
			{
				rb.useGravity = false;
				//rb.isKinematic = false;
			}
			else
			{
				rb.useGravity = false;
				rb.isKinematic = true;
			}*/
			
			
			
			/*
			if (grab01.isGrabbed)
			{
				gameObject.GetComponent<Collider>().isTrigger = true;
			}
			else
			{
				gameObject.GetComponent<Collider>().isTrigger = false;
			}*/
			
			
			
			
			/*if (grab01.isGrabbed)
			{
				ControlsDisplay.SetActive(true);
			}
			else{
				ControlsDisplay.SetActive(false);
			}*/
			
			
			
			
			
			A_Button = OVRInput.Get(OVRInput.RawButton.A);
			B_Button = OVRInput.Get(OVRInput.RawButton.B);
			X_Button = OVRInput.Get(OVRInput.RawButton.X);
			Y_Button = OVRInput.Get(OVRInput.RawButton.Y);
			LTriggerButton = OVRInput.Get(OVRInput.Axis1D.PrimaryIndexTrigger, OVRInput.Controller.Touch);
			RTriggerButton = OVRInput.Get(OVRInput.Axis1D.SecondaryIndexTrigger, OVRInput.Controller.Touch);
			
			
			//Debug.Log(appleRenderer.material);
			/*if (enlargeButton && grab01.isGrabbed)
			{
				
				//ChangeAppleColor();
				IncreaseAppleSize();
			}
			
			if (reduceButton && grab01.isGrabbed)
			{
				
				//ChangeAppleColor();
				DecreaseAppleSize();
			}*/
			
			if (hand != null)
			{
				if (hand.tag == "left hand")
				{
					if (X_Button && grab01.isGrabbed)
					{
						IncreaseAppleSize();
					}
					
					if (Y_Button && grab01.isGrabbed)
					{
						DecreaseAppleSize();
					}
					
					if (LTriggerButton >= 0.2f && grab01.isGrabbed && !IsColorChanged)
					{
						ColorNumber++;
						ChangeAppleColorRPC(ColorNumber);
						if (ColorNumber >= 5)
						{
							ColorNumber = 0;
						}
						IsColorChanged = true;
					}
					if (LTriggerButton == 0)
					{
						IsColorChanged = false;
					}
				}
				else if (hand.tag == "right hand")
				{
					if (A_Button && grab01.isGrabbed)
					{
						IncreaseAppleSize();
					}
					
					if (B_Button && grab01.isGrabbed)
					{
						DecreaseAppleSize();
					}
					
					if (RTriggerButton >= 0.2f && grab01.isGrabbed && !IsColorChanged)
					{
						ColorNumber++;
						ChangeAppleColorRPC(ColorNumber);
						if (ColorNumber >= 5)
						{
							ColorNumber = 0;
						}
						IsColorChanged = true;
					}
					if (RTriggerButton == 0)
					{
						IsColorChanged = false;
					}
				}
			}
			
			//Debug.Log("" + LTriggerButton + " " + RTriggerButton);
			/*if ((LTriggerButton >= 0.5f || RTriggerButton >= 0.5f) && grab01.isGrabbed && !IsColorChanged)
			{
				ColorNumber++;
				ChangeAppleColorRPC(ColorNumber);
				if (ColorNumber >= 5)
				{
					ColorNumber = 0;
				}
				IsColorChanged = true;
			}*/
			
			if (LTriggerButton == 0 && RTriggerButton == 0)
			{
				IsColorChanged = false;
			}
		}
		
		
		
		private void IncreaseAppleSize()
		{
			//if (photonView.IsMine)
			//{
				gameObject.transform.localScale += new Vector3(0.1f,0.1f,0.1f);
				GetComponent<PhotonView>().RPC("RPC_ChangeMass", RpcTarget.AllBuffered, 0.1f);
				
				//photonView.RPC("RPC_SetColor", RpcTarget.AllBuffered, newAppleColor);
			//}
		}
		
		private void DecreaseAppleSize()
		{
			//if (photonView.IsMine)
			//{
				gameObject.transform.localScale -= new Vector3(0.1f,0.1f,0.1f);
				GetComponent<PhotonView>().RPC("RPC_ChangeMass", RpcTarget.AllBuffered, -0.1f);
				
				//photonView.RPC("RPC_SetColor", RpcTarget.AllBuffered, newAppleColor);
			//}
		}
		
		/*[PunRPC]
		void RPC_SetColor(Color transferredColor)
		{
			appleRenderer.material.color = transferredColor;
		}*/
		
		private void OnCollisionEnter(Collision col)
		{
			if (col.gameObject.tag == "rake" || col.gameObject.tag == "pick box" || col.gameObject.tag == "apple" || col.gameObject.tag == "human")
			{
				rb.useGravity = true;
			}
		}
		
		
		
		
		public void ChangeAppleColorRPC(int n)
		{
			GetComponent<PhotonView>().RPC("RPC_SetColor", RpcTarget.AllBuffered, n);
			
			/*channelOne = Random.Range(0f, 1f);
			channelTwo = Random.Range(0f, 1f);
			channelThree = Random.Range(0f, 1f);

			newAppleColor = new Color(channelOne, channelTwo, channelThree, 1f);

			appleRenderer.material.SetColor("_Color", newAppleColor);*/
		}
		
		[PunRPC]
		void RPC_SetColor(int n)
		{
			switch (n)
				{
					case 1:
						AppleColor = Color.red;
						break;
					case 2:
						AppleColor = Color.cyan;
						break;
					case 3:
						AppleColor = Color.green;
						break;
					case 4:
						AppleColor = Color.yellow;
						break;
					case 5:
						AppleColor = Color.magenta;
						break;
				}
				channelOne = Random.Range(0f, 1f);
				channelTwo = Random.Range(0f, 1f);
				channelThree = Random.Range(0f, 1f);
				//AppleColor = Color.Lerp(Color.white, AppleColor, 0.5f);
				//AppleColor = new Color(channelOne, channelTwo, channelThree, 1f);
				appleRenderer.material.color = AppleColor;
		}
		
		[PunRPC]
		void RPC_ChangeMass(float n)
		{
			rb.mass += n;
		}
	}
}
