﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Photon.Realtime;
using Photon.Pun;

public class BoxController : MonoBehaviourPun
{
	private Rigidbody rb;
	private OVRGrabbable grab01;
	
	public GameObject[] AllColliders;
	public int GrabbableLayer;
	public int DefaultLayer;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        grab01 = GetComponent<OVRGrabbable>();
		
		/*
		if (grab01.grabDone)
		{
			rb.useGravity = true;
			rb.isKinematic = false;
		}
		else if (grab01.isGrabbed)
		{
			rb.useGravity = false;
			rb.isKinematic = false;
		}
		else
		{
			rb.useGravity = false;
			rb.isKinematic = true;
		}*/
		
		if (grab01.isGrabbed)
		{
			for (int i = 0; i < AllColliders.Length; i++)
			{
				AllColliders[i].layer = GrabbableLayer;
				//AllColliders[i].GetComponent<Collider>().isTrigger = true;
			}
		}
		else
		{
			for (int i = 0; i < AllColliders.Length; i++)
			{
				AllColliders[i].layer = DefaultLayer;
				//AllColliders[i].GetComponent<Collider>().isTrigger = false;
			}
		}
    }
}
