﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Photon.Pun;
using Photon.Realtime;

public class CheckPhoton : MonoBehaviourPun
{
    // Start is called before the first frame update
    void Start()
    {
        if (photonView.IsMine == false)
		{
			for(int i = 0; i < transform.childCount; i++)
			{
				transform.GetChild(i).gameObject.SetActive(false);
			}
			GetComponent<OVRDebugInfo>().enabled = false;
			GetComponent<CharacterCameraConstraint>().enabled = false;
			GetComponent<SimpleCapsuleWithStickMovement>().enabled = false;
			GetComponent<Collider>().enabled = false;
		}
		
		else if (photonView.IsMine == true)
		{
			for(int i = 0; i < transform.childCount; i++)
			{
				transform.GetChild(i).gameObject.SetActive(true);
			}
			GetComponent<OVRDebugInfo>().enabled = true;
			GetComponent<CharacterCameraConstraint>().enabled = true;
			GetComponent<SimpleCapsuleWithStickMovement>().enabled = true;
			GetComponent<Collider>().enabled = true;
		}
    }

    // Update is called once per frame
    void Update()
    {
        if (photonView.IsMine == false)
		{
			for(int i = 0; i < transform.childCount; i++)
			{
				transform.GetChild(i).gameObject.SetActive(false);
			}
			GetComponent<OVRDebugInfo>().enabled = false;
			GetComponent<CharacterCameraConstraint>().enabled = false;
			GetComponent<SimpleCapsuleWithStickMovement>().enabled = false;
			GetComponent<Collider>().enabled = false;
		}
		
		else if (photonView.IsMine == true)
		{
			for(int i = 0; i < transform.childCount; i++)
			{
				transform.GetChild(i).gameObject.SetActive(true);
			}
			GetComponent<OVRDebugInfo>().enabled = true;
			GetComponent<CharacterCameraConstraint>().enabled = true;
			GetComponent<SimpleCapsuleWithStickMovement>().enabled = true;
			GetComponent<Collider>().enabled = true;
		}
    }
}

