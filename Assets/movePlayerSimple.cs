﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Photon.Pun;
using Photon.Realtime;

public class movePlayerSimple : MonoBehaviourPun
{
	
	public bool EnableLinearMovement = true;
	public bool EnableRotation = true;
	public float RotationAngle = 45.0f;
	public bool HMDRotatesPlayer = true;
	public float Speed = 0.0f;
	
	public bool HmdResetsY = true;
	
	/// <summary>
	/// If true, each OVRPlayerController will use the player's physical height.
	/// </summary>
	public bool useProfileData = true;
	
	public OVRCameraRig CameraRig;
	
	
	private bool ReadyToSnapTurn;
	
	

    // Start is called before the first frame update
    void Start()
    {
        if (useProfileData)
		{
			//if (photonView.IsMine)
			//{
				var p = CameraRig.transform.localPosition;
				if (OVRManager.instance.trackingOriginType == OVRManager.TrackingOrigin.EyeLevel)
				{
					p.y = OVRManager.profile.eyeHeight - (0.6f * gameObject.GetComponent<CapsuleCollider>().height) + gameObject.GetComponent<CapsuleCollider>().center.y;
				}
				else if (OVRManager.instance.trackingOriginType == OVRManager.TrackingOrigin.FloorLevel)
				{
					p.y = -(0.6f * gameObject.GetComponent<CapsuleCollider>().height) + gameObject.GetComponent<CapsuleCollider>().center.y;
				}
				CameraRig.transform.localPosition = p;
			//}
		}
    }

    // Update is called once per frame
    void Update()
    {
		//Debug.Log(OVRManager.instance.trackingOriginType);
		//if (photonView.IsMine)
		//{
			if (HMDRotatesPlayer) RotatePlayerToHMD();
			if (EnableLinearMovement) StickMovement();
			if (EnableRotation) SnapTurn();
		//}
    }
	
	
	void RotatePlayerToHMD()
    {
		Transform root = CameraRig.trackingSpace;
		Transform centerEye = CameraRig.centerEyeAnchor;

		Vector3 prevPos = root.position;
		Quaternion prevRot = root.rotation;

		transform.rotation = Quaternion.Euler(0.0f, centerEye.rotation.eulerAngles.y, 0.0f);

		root.position = prevPos;
		root.rotation = prevRot;
    }

	void StickMovement()
	{
		Quaternion ort = CameraRig.centerEyeAnchor.rotation;
		Vector3 ortEuler = ort.eulerAngles;
		ortEuler.z = ortEuler.x = 0f;
		ort = Quaternion.Euler(ortEuler);

		Vector3 moveDir = Vector3.zero;
		
		
		Vector2 primaryAxis = OVRInput.Get(OVRInput.RawAxis2D.RThumbstick);
		//Vector2 primaryAxis = OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick);
		moveDir += ort * (primaryAxis.x * Vector3.right);
		moveDir += ort * (primaryAxis.y * Vector3.forward);
		//Debug.Log(OVRInput.Get(OVRInput.RawButton.LIndexTrigger));
		//_rigidbody.MovePosition(_rigidbody.transform.position + moveDir * Speed * Time.fixedDeltaTime);
		gameObject.transform.position = gameObject.transform.position + moveDir * Speed * Time.fixedDeltaTime;
		//gameObject.MovePosition(gameObject.position + moveDir * Speed * Time.fixedDeltaTime);
	}
	
	
	
	void SnapTurn()
	{
		/*
		if (OVRInput.Get(OVRInput.Button.SecondaryThumbstickLeft) ||
			(RotationEitherThumbstick && OVRInput.Get(OVRInput.Button.PrimaryThumbstickLeft)))
		{
			if (ReadyToSnapTurn)
			{
				ReadyToSnapTurn = false;
				transform.RotateAround(CameraRig.centerEyeAnchor.position, Vector3.up, -RotationAngle);
			}
		}
		else if (OVRInput.Get(OVRInput.Button.SecondaryThumbstickRight) ||
			(RotationEitherThumbstick && OVRInput.Get(OVRInput.Button.PrimaryThumbstickRight)))
		{
			if (ReadyToSnapTurn)
			{
				ReadyToSnapTurn = false;
				transform.RotateAround(CameraRig.centerEyeAnchor.position, Vector3.up, RotationAngle);
			}
		}
		else
		{
			ReadyToSnapTurn = true;
		}
		*/
		
		
		
		
		if (OVRInput.Get(OVRInput.Button.PrimaryThumbstickLeft,OVRInput.Controller.LTouch))
		{
			if (ReadyToSnapTurn)
			{
				//ReadyToSnapTurn = false;
				transform.RotateAround(CameraRig.centerEyeAnchor.position, Vector3.up, -RotationAngle);
			}
		}
		else if (OVRInput.Get(OVRInput.Button.PrimaryThumbstickRight,OVRInput.Controller.LTouch))
		{
			if (ReadyToSnapTurn)
			{
				//ReadyToSnapTurn = false;
				transform.RotateAround(CameraRig.centerEyeAnchor.position, Vector3.up, RotationAngle);
			}
		}
		else
		{
			ReadyToSnapTurn = true;
		}
	}
}
