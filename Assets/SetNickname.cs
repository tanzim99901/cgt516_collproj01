﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using Photon.Pun;
using Photon.Realtime;

namespace Com.CGT516.ApplePicker
{
	public class SetNickname : MonoBehaviourPun
	{
		public Text DisplayText;
		public string PlayerName = "";
		
		// Start is called before the first frame update
		/*void Start()
		{
			if (photonView.IsMine)
			{
				DisplayText.text = PlayerName;
			}
		}

		// Update is called once per frame
		void Update()
		{
			if (photonView.IsMine)
			{
				DisplayText.text = PlayerName;
			}
		}*/
		
		public void SetNicknameRPC(string name)
		{
			GetComponent<PhotonView>().RPC("RPC_SetNickname", RpcTarget.AllBuffered, name);
		}
		
		[PunRPC]
		void RPC_SetNickname(string name)
		{
			PlayerName = name;
			DisplayText.text = PlayerName;
			//DisplayText.color = Color.white;
		}
	}
}
