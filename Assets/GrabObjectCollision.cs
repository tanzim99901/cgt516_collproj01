﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabObjectCollision : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
	
	private void OnCollisionEnter(Collision col)
	{
		if (col.gameObject.tag == "pick box" || col.gameObject.tag == "rake" || col.gameObject.tag == "apple")
		{
			col.gameObject.GetComponent<Collider>().isTrigger = true;
		}
	}
	
	private void OnCollisionExit(Collision col)
	{
		if (col.gameObject.tag == "pick box" || col.gameObject.tag == "rake" || col.gameObject.tag == "apple")
		{
			col.gameObject.GetComponent<Collider>().isTrigger = false;
		}
	}
}
