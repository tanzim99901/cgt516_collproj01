﻿using UnityEngine;
using Photon.Realtime;
using Photon.Pun;
using UnityEngine.SceneManagement;
using System.Collections;
using UnityEngine.UI;
using TMPro;

//
//This script connects to PHOTON servers and creates a room if there is none, then automatically joins
//
namespace Com.CGT516.ApplePicker
{
    public class NewNetworkManager : MonoBehaviourPunCallbacks
    {
		private bool LevelSelected = false;
		
		private bool DaySelected = false;
		private bool NightSelected = false;
		
		public bool EnableInput = false;
		
		/// <summary>
		/// The maximum number of players per room. When a room is full, it can't be joined by new players, and so new room will be created.
		/// </summary>
		[Tooltip("The maximum number of players per room. When a room is full, it can't be joined by new players, and so new room will be created")]
		[SerializeField]
		private byte maxPlayersPerRoom = 5;
		
		[Tooltip("The name of the room to create or join")]
		[SerializeField]
		private string RoomName = "Z_World";
		
		[Tooltip("The name of the room for Daytime")]
		[SerializeField]
		private string DayRoomName = "Z_World_Day";
		
		[Tooltip("The name of the room Nighttime")]
		[SerializeField]
		private string NightRoomName = "Z_World_Night";
		
		
		[Tooltip("The Ui Panel to let the user enter name, connect and play")]
		[SerializeField]
		private GameObject controlPanel;
		
		[Tooltip("The UI Label to inform the user that the connection is in progress")]
		[SerializeField]
		private GameObject progressLabel;
		
		[Tooltip("The UI Label to inform the user that the connection is succesful")]
		[SerializeField]
		private GameObject connectedLabel;
		
		[Tooltip("The UI Label to select the level")]
		[SerializeField]
		private GameObject LevelSelectLabel;
		
		[Tooltip("The day selection")]
		[SerializeField]
		private GameObject DaySelection;
		
		[Tooltip("The night selection")]
		[SerializeField]
		private GameObject NightSelection;
		
		
		[Tooltip("Player name input")]
		[SerializeField]
		private GameObject nameInput;
		
		private string playerName;
		
		/// <summary>
        /// This client's version number. Users are separated from each other by gameVersion (which allows you to make breaking changes).
        /// </summary>
        string gameVersion = "v1";
		/// <summary>
		/// Keep track of the current process. Since connection is asynchronous and is based on several callbacks from Photon,
		/// we need to keep track of this to properly adjust the behavior when we receive call back by Photon.
		/// Typically this is used for the OnConnectedToMaster() callback.
		/// </summary>
		bool isConnecting;
		
        bool triesToConnectToMaster = false;
        bool triesToConnectToRoom = false;
		
		bool enterPressed = false;
		
		
		// <summary>
        /// MonoBehaviour method called on GameObject by Unity during initialization phase.
        /// </summary>
        void Start()
        {
            //Connect();
			progressLabel.SetActive(false);
			LevelSelectLabel.SetActive(true);
			DaySelection.SetActive(true);
			DaySelected = true;
			NightSelection.SetActive(false);
			connectedLabel.SetActive(false);
			controlPanel.SetActive(false);
			
			//Connect();
        }
		
		
		
        private void Update()
        {
			
            if (!PhotonNetwork.IsConnected && !triesToConnectToMaster && enterPressed)
            {
                ConnectToMaster();
            }
            if (PhotonNetwork.IsConnected && !triesToConnectToMaster && !triesToConnectToRoom && enterPressed)
            {
                StartCoroutine(WaitFrameAndConnect());
            }
        }
		
		public void loginSwitch()
		{
			if (LevelSelected)
			{
				enterPressed = true;
			}
			else
			{
				LevelSelected = true;
				LevelSelectLabel.SetActive(false);
				DaySelection.SetActive(false);
				NightSelection.SetActive(false);
				controlPanel.SetActive(true);
				progressLabel.SetActive(false);
				connectedLabel.SetActive(false);
				EnableInput = true;
				print("Level Selected");
				SetRoomName();
			}
		}
		
		public void SetRoomName()
		{
			if (DaySelected)
			{
				RoomName = DayRoomName;
				print("Room set to day");
			}
			else if (NightSelected)
			{
				RoomName = NightRoomName;
				print("Room set to night");
			}
		}
		
		public void getPlayerName()
		{
			playerName = nameInput.GetComponent<TMP_InputField>().text;;
			//Debug.Log(playerName);
		}
		
        public void ConnectToMaster()
        {
            PhotonNetwork.OfflineMode = false; //true would "fake" an online connection
			getPlayerName();
            PhotonNetwork.NickName = playerName; //we can use a input to change this 
            PhotonNetwork.AutomaticallySyncScene = true; //To call PhotonNetwork.LoadLevel()
            PhotonNetwork.GameVersion = gameVersion; //only people with the same game version can play together

            triesToConnectToMaster = true;
            //PhotonNetwork.ConnectToMaster(ip, port, appid); //manual connection
            PhotonNetwork.ConnectUsingSettings(); //automatic connection based on the config file
        }

        public override void OnDisconnected(DisconnectCause cause)
        {
            base.OnDisconnected(cause);
            triesToConnectToMaster = false;
            triesToConnectToRoom = false;
			progressLabel.SetActive(false);
			controlPanel.SetActive(true);
			connectedLabel.SetActive(false);
            Debug.Log(cause);
        }

        public override void OnConnectedToMaster()
        {
            base.OnConnectedToMaster();
            triesToConnectToMaster = false;
            Debug.Log("Connected to master!");
        }

        IEnumerator WaitFrameAndConnect()
        {
            triesToConnectToRoom = true;
            yield return new WaitForEndOfFrame();
            Debug.Log("Connecting");
			progressLabel.SetActive(true);
			connectedLabel.SetActive(false);
			controlPanel.SetActive(false);
            ConnectToRoom();
        }

        public void ConnectToRoom()
        {
            if (!PhotonNetwork.IsConnected)
                return;

            triesToConnectToRoom = true;
			RoomOptions options = new RoomOptions();
			options.MaxPlayers = maxPlayersPerRoom;
			PhotonNetwork.JoinOrCreateRoom(RoomName, options, TypedLobby.Default);
            //PhotonNetwork.JoinOrCreateRoom(RoomName); //Create a specific room - Callback OnCreateRoomFailed
            //PhotonNetwork.JoinRoom(RoomName); //Join a specific room - Callback OnJoinRoomFailed

            //PhotonNetwork.JoinRandomRoom(); // Join a random room - Callback OnJoinRandomRoomFailed
        }

        public override void OnJoinedRoom()
        {
            //Go to next scene after joining the room
			progressLabel.SetActive(false);
			connectedLabel.SetActive(true);
			controlPanel.SetActive(false);
            base.OnJoinedRoom();
            Debug.Log("Master: " + PhotonNetwork.IsMasterClient + " | Players In Room: " + PhotonNetwork.CurrentRoom.PlayerCount + " | RoomName: " + PhotonNetwork.CurrentRoom.Name + " Region: " + PhotonNetwork.CloudRegion);
            //Destroy(GameObject.Find("NewPlayerTanzim"));
			//LevelSelected = true;
			//connectedLabel.SetActive(false);
			//LevelSelectLabel.SetActive(true);
			//DaySelection.SetActive(true);
			//NightSelection.SetActive(false);
			//DaySelected = true;
			//NightSelected = false;
            //SceneManager.LoadScene(1); //go to the room scene
			LoadSelectedLevel();
        }

        public override void OnJoinRandomFailed(short returnCode, string message)
        {
            base.OnJoinRandomFailed(returnCode, message);
            //no room available
            //create a room (null as a name means "does not matter")
            PhotonNetwork.CreateRoom(RoomName, new RoomOptions { MaxPlayers = maxPlayersPerRoom });
        }
		
		
		
		
		// For level loading
		public void LoadSelectedLevel()
		{
			if (DaySelected)
			{
				SceneManager.LoadScene(1);
			}
			else if (NightSelected)
			{
				SceneManager.LoadScene(2);
			}
		}
		
		// For level selection
		public void SwitchLevel()
		{
			if (DaySelected)
			{
				DaySelection.SetActive(false);
				NightSelection.SetActive(true);
				DaySelected = false;
				NightSelected = true;
			}
			else if (NightSelected)
			{
				DaySelection.SetActive(true);
				NightSelection.SetActive(false);
				DaySelected = true;
				NightSelected = false;
			}
		}
    }
}