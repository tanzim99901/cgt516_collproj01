﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Com.CGT516.ApplePicker
{
	public class AxeController : MonoBehaviour
	{
		private Rigidbody rb;
		private PunOVRGrabbable grab01;
		
		public GameObject AllColliders;
		public int GrabbableLayer;
		public int DefaultLayer;

		void Start()
		{
			rb = GetComponent<Rigidbody>();
		}

		void Update()
		{
			grab01 = GetComponent<PunOVRGrabbable>();
			
			if (grab01.isGrabbed)
			{
				AllColliders.layer = GrabbableLayer;
			}
			else
			{
				AllColliders.layer = DefaultLayer;
			}
			
			if (grab01.isGrabbed)
			{
				if (gameObject.GetComponent<FixedJoint>() != null)
				{
					if (OVRInput.Get(OVRInput.RawButton.B))
					{
						//gameObject.GetComponent<FixedJoint>().enabled = false;
						Destroy(gameObject.GetComponent<FixedJoint>());
					}
				}
			}
		}
		
		void OnCollisionEnter(Collision col)
		 {
			 if (col.gameObject.tag == "Log block" && OVRInput.Get(OVRInput.RawButton.A))
			 {
				 // creates joint
				 FixedJoint joint = gameObject.AddComponent<FixedJoint>(); 
				 // sets joint position to point of contact
				 joint.anchor = col.contacts[0].point; 
				 // conects the joint to the other object
				 joint.connectedBody = col.contacts[0].otherCollider.transform.GetComponentInParent<Rigidbody>(); 
				 // Stops objects from continuing to collide and creating more joints
				 joint.enableCollision = false;
				 
				 
			 }
		 }
	}
}
