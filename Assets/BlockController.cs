﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Photon.Realtime;
using Photon.Pun;

namespace Com.CGT516.ApplePicker
{
	public class BlockController : MonoBehaviourPun
	{
		private Rigidbody rb;
		private PunOVRGrabbable grab01;
		
		public GameObject AllColliders;
		public int GrabbableLayer;
		public int DefaultLayer;
		
		private Collision col01;

		void Start()
		{
			rb = GetComponent<Rigidbody>();
		}

		void Update()
		{
			grab01 = GetComponent<PunOVRGrabbable>();
			
			if (grab01.isGrabbed)
			{
				//AllColliders.layer = GrabbableLayer;
			}
			else
			{
				//AllColliders.layer = DefaultLayer;
			}
			
			if (grab01.isGrabbed)
			{
				if (gameObject.GetComponent<FixedJoint>() != null)
				{
					if (OVRInput.Get(OVRInput.RawButton.B))
					{
						//gameObject.GetComponent<FixedJoint>().enabled = false;
						RemoveJointRPC(1);
						//Destroy(gameObject.GetComponent<FixedJoint>());
					}
				}
			}
		}
		
		void OnCollisionEnter(Collision col)
		{
			if (col.gameObject.tag == "Connection Collider" && OVRInput.Get(OVRInput.RawButton.A))
			{
				col01 = col;
				AddJointRPC(1);
				
				/*
				// creates joint
				FixedJoint joint = gameObject.AddComponent<FixedJoint>(); 
				// sets joint position to point of contact
				joint.anchor = col.contacts[0].point; 
				// conects the joint to the other object
				joint.connectedBody = col.contacts[0].otherCollider.transform.GetComponentInParent<Rigidbody>(); 
				// Stops objects from continuing to collide and creating more joints
				joint.enableCollision = false;
				*/
				 
				 
			}
		}
		
		public void RemoveJointRPC(int n)
		{
			GetComponent<PhotonView>().RPC("RPC_RemoveJoint", RpcTarget.AllBuffered, n);
		}
		
		public void AddJointRPC(int n)
		{
			GetComponent<PhotonView>().RPC("RPC_AddJoint", RpcTarget.AllBuffered, n);
		}
		
		[PunRPC]
		void RPC_RemoveJoint(int n)
		{
			Destroy(gameObject.GetComponent<FixedJoint>());
		}
		
		[PunRPC]
		void RPC_AddJoint(int n)
		{
			// creates joint
			FixedJoint joint = gameObject.AddComponent<FixedJoint>(); 
			// sets joint position to point of contact
			joint.anchor = col01.contacts[0].point; 
			// conects the joint to the other object
			joint.connectedBody = col01.contacts[0].otherCollider.transform.GetComponentInParent<Rigidbody>(); 
			// Stops objects from continuing to collide and creating more joints
			joint.enableCollision = false;
		}
	}
}
