﻿using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.UI;


namespace Com.CGT516.ApplePicker
{
    public class Launcher : MonoBehaviourPunCallbacks
    {
		private string gameRoom = "GameRoom_01_Tanzim";
        #region Private Serializable Fields

		
		/// <summary>
		/// The maximum number of players per room. When a room is full, it can't be joined by new players, and so new room will be created.
		/// </summary>
		[Tooltip("The maximum number of players per room. When a room is full, it can't be joined by new players, and so new room will be created")]
		[SerializeField]
		private byte maxPlayersPerRoom = 4;
		
		[Tooltip("The name of the room to create or join")]
		[SerializeField]
		private string _roomName;
		
		
		[Tooltip("The Ui Panel to let the user enter name, connect and play")]
		[SerializeField]
		private GameObject controlPanel;
		
		[Tooltip("The UI Label to inform the user that the connection is in progress")]
		[SerializeField]
		private GameObject progressLabel;
		
		[Tooltip("The UI Label to inform the user that the connection is succesful")]
		[SerializeField]
		private GameObject connectedLabel;

        #endregion


        #region Private Fields


        /// <summary>
        /// This client's version number. Users are separated from each other by gameVersion (which allows you to make breaking changes).
        /// </summary>
        string gameVersion = "1";
		/// <summary>
		/// Keep track of the current process. Since connection is asynchronous and is based on several callbacks from Photon,
		/// we need to keep track of this to properly adjust the behavior when we receive call back by Photon.
		/// Typically this is used for the OnConnectedToMaster() callback.
		/// </summary>
		bool isConnecting;


        #endregion


        #region MonoBehaviour CallBacks


        /// <summary>
        /// MonoBehaviour method called on GameObject by Unity during early initialization phase.
        /// </summary>
        void Awake()
        {
            // #Critical
            // this makes sure we can use PhotonNetwork.LoadLevel() on the master client and all clients in the same room sync their level automatically
            PhotonNetwork.AutomaticallySyncScene = true;
        }


        /// <summary>
        /// MonoBehaviour method called on GameObject by Unity during initialization phase.
        /// </summary>
        void Start()
        {
            //Connect();
			progressLabel.SetActive(false);
			connectedLabel.SetActive(false);
			controlPanel.SetActive(true);
			
			//Connect();
        }


        #endregion


        #region Public Methods


        /// <summary>
        /// Start the connection process.
        /// - If already connected, we attempt joining a random room
        /// - if not yet connected, Connect this application instance to Photon Cloud Network
        /// </summary>
        public void Connect()
        {
			// keep track of the will to join a room, because when we come back from the game we will get a callback that we are connected, so we need to know what to do then
			isConnecting = PhotonNetwork.ConnectUsingSettings();
			
			
			
			progressLabel.SetActive(true);
			connectedLabel.SetActive(false);
			controlPanel.SetActive(false);
            // we check if we are connected or not, we join if we are , else we initiate the connection to the server.
            if (PhotonNetwork.IsConnected)
            {
                // #Critical we need at this point to attempt joining a Random Room. If it fails, we'll get notified in OnJoinRandomFailed() and we'll create one.
                //PhotonNetwork.JoinRandomRoom();
				
				
				/*
				
				//// Create room
				RoomOptions options = new RoomOptions();
				options.MaxPlayers = maxPlayersPerRoom;
				PhotonNetwork.JoinOrCreateRoom(_roomName, options, TypedLobby.Default);
				/// Create room
				
				*/
				
				Debug.Log("Connected....Yeah!!");
            }
            else
            {
                // #Critical, we must first and foremost connect to Photon Online Server.
                // keep track of the will to join a room, because when we come back from the game we will get a callback that we are connected, so we need to know what to do then
				isConnecting = PhotonNetwork.ConnectUsingSettings();
                PhotonNetwork.GameVersion = gameVersion;
            }
        }
		
		#region MonoBehaviourPunCallbacks Callbacks


		public override void OnConnectedToMaster()
		{
			Debug.Log("PUN Basics Tutorial/Launcher: OnConnectedToMaster() was called by PUN");
			// #Critical: The first we try to do is to join a potential existing room. If there is, good, else, we'll be called back with OnJoinRandomFailed()
			if (isConnecting)
			{
				//PhotonNetwork.JoinRandomRoom();
				
				
				//// Create room
				RoomOptions options = new RoomOptions();
				options.MaxPlayers = maxPlayersPerRoom;
				PhotonNetwork.JoinOrCreateRoom(_roomName, options, TypedLobby.Default);
				/// Create room
				
				
				isConnecting = false;
			}
		}


		public override void OnDisconnected(DisconnectCause cause)
		{
			progressLabel.SetActive(false);
			controlPanel.SetActive(true);
			connectedLabel.SetActive(false);
			isConnecting = false;
			Debug.LogWarningFormat("PUN Basics Tutorial/Launcher: OnDisconnected() was called by PUN with reason {0}", cause);
		}
		
		
		public override void OnJoinRandomFailed(short returnCode, string message)
		{
			Debug.Log("PUN Basics Tutorial/Launcher:OnJoinRandomFailed() was called by PUN. No random room available, so we create one.\nCalling: PhotonNetwork.CreateRoom");

			// #Critical: we failed to join a random room, maybe none exists or they are all full. No worries, we create a new room.
			//PhotonNetwork.CreateRoom(null, new RoomOptions { MaxPlayers = maxPlayersPerRoom });
			
			
			
			//// Create room
				RoomOptions options = new RoomOptions();
				options.MaxPlayers = maxPlayersPerRoom;
				PhotonNetwork.JoinOrCreateRoom(_roomName, options, TypedLobby.Default);
				/// Create room
		}

		public override void OnJoinedRoom()
		{
			Debug.Log("PUN Basics Tutorial/Launcher: OnJoinedRoom() called by PUN. Now this client is in a room.");
			Debug.Log(PhotonNetwork.CurrentRoom.Name);
			Debug.Log(PhotonNetwork.CurrentRoom.PlayerCount);
			progressLabel.SetActive(false);
			connectedLabel.SetActive(true);
			
			
			
			
			
			///// New code
			// #Critical: We only load if we are the first player, else we rely on `PhotonNetwork.AutomaticallySyncScene` to sync our instance scene.
			if (PhotonNetwork.CurrentRoom.PlayerCount <= maxPlayersPerRoom)
			{
				Debug.Log("We load the 'GameRoom_01_Tanzim' ");


				// #Critical
				// Load the Room Level.
				PhotonNetwork.LoadLevel(gameRoom);
			}
		}

		#endregion


    #endregion


    }
}