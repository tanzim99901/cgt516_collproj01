using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;


public class SimpleCapsuleWithStickMovement : MonoBehaviour
{
	public bool EnableLinearMovement = true;
	public bool EnableRotation = true;
	public bool HMDRotatesPlayer = true;
	public bool RotationEitherThumbstick = false;
	public bool EnableFlight = false;
	public float RotationAngle = 45.0f;
	public float Speed = 0.0f;
	public OVRCameraRig CameraRig;

	private bool ReadyToSnapTurn;
	private Rigidbody _rigidbody;

	public event Action CameraUpdated;
	public event Action PreCharacterMove;
	
	
	// Flight variables
	public float flyingSpeed;
	public float freefallSpeed;
	public float FlightMovementSpeed;
	public float FlightRotationAngle;
	
	private bool isFlying = false;
	private bool isFalling = false;
	private bool flyingStarted = false;

	private void Awake()
	{

			_rigidbody = GetComponent<Rigidbody>();
			if (CameraRig == null) CameraRig = GetComponentInChildren<OVRCameraRig>();

	}

	void Start ()
	{
		
	}
	
	private void FixedUpdate()
	{

			if (CameraUpdated != null) CameraUpdated();
			if (PreCharacterMove != null) PreCharacterMove();

			if (HMDRotatesPlayer) RotatePlayerToHMD();
			if (EnableLinearMovement && !EnableFlight) StickMovement();
			if (EnableRotation && !EnableFlight) SnapTurn();
			if (EnableFlight) flight();

	}

    void RotatePlayerToHMD()
    {
		Transform root = CameraRig.trackingSpace;
		Transform centerEye = CameraRig.centerEyeAnchor;

		Vector3 prevPos = root.position;
		Quaternion prevRot = root.rotation;

		transform.rotation = Quaternion.Euler(0.0f, centerEye.rotation.eulerAngles.y, 0.0f);

		root.position = prevPos;
		root.rotation = prevRot;
    }

	void StickMovement()
	{
		Quaternion ort = CameraRig.centerEyeAnchor.rotation;
		Vector3 ortEuler = ort.eulerAngles;
		ortEuler.z = ortEuler.x = 0f;
		ort = Quaternion.Euler(ortEuler);

		Vector3 moveDir = Vector3.zero;
		//Vector2 primaryAxis = OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick,OVRInput.Controller.RTouch);
		Vector2 primaryAxis = OVRInput.Get(OVRInput.RawAxis2D.RThumbstick);
		moveDir += ort * (primaryAxis.x * Vector3.right);
		moveDir += ort * (primaryAxis.y * Vector3.forward);
		//Debug.Log(OVRInput.Get(OVRInput.RawButton.LIndexTrigger));
		//_rigidbody.MovePosition(_rigidbody.transform.position + moveDir * Speed * Time.fixedDeltaTime);
		_rigidbody.MovePosition(_rigidbody.position + moveDir * Speed * Time.fixedDeltaTime);
	}

	void SnapTurn()
	{
		/*
		if (OVRInput.Get(OVRInput.Button.SecondaryThumbstickLeft) ||
			(RotationEitherThumbstick && OVRInput.Get(OVRInput.Button.PrimaryThumbstickLeft)))
		{
			if (ReadyToSnapTurn)
			{
				ReadyToSnapTurn = false;
				transform.RotateAround(CameraRig.centerEyeAnchor.position, Vector3.up, -RotationAngle);
			}
		}
		else if (OVRInput.Get(OVRInput.Button.SecondaryThumbstickRight) ||
			(RotationEitherThumbstick && OVRInput.Get(OVRInput.Button.PrimaryThumbstickRight)))
		{
			if (ReadyToSnapTurn)
			{
				ReadyToSnapTurn = false;
				transform.RotateAround(CameraRig.centerEyeAnchor.position, Vector3.up, RotationAngle);
			}
		}
		else
		{
			ReadyToSnapTurn = true;
		}
		*/
		
		
		
		
		if (OVRInput.Get(OVRInput.Button.PrimaryThumbstickLeft,OVRInput.Controller.LTouch))
		{
			if (ReadyToSnapTurn)
			{
				//ReadyToSnapTurn = false;
				transform.RotateAround(CameraRig.centerEyeAnchor.position, Vector3.up, -RotationAngle);
			}
		}
		else if (OVRInput.Get(OVRInput.Button.PrimaryThumbstickRight,OVRInput.Controller.LTouch))
		{
			if (ReadyToSnapTurn)
			{
				//ReadyToSnapTurn = false;
				transform.RotateAround(CameraRig.centerEyeAnchor.position, Vector3.up, RotationAngle);
			}
		}
		else
		{
			ReadyToSnapTurn = true;
		}
	}
	
	
	#region Flight functions
	
	void flight()
	{
		
		CheckIfFlying();
		FlyIfFlying();
		FlightStickMovement();
		FlightSnapTurn();
		FreeFall();
	}
	
	
	private void CheckIfFlying()
	{
		if (OVRInput.Get(OVRInput.Button.PrimaryIndexTrigger, OVRInput.Controller.LTouch))
		{
			isFlying = true;
			flyingStarted = true;
			Destroy(_rigidbody);
			//_rigidbody.useGravity = false;
			//print(_rigidbody.useGravity);
		}
		if (OVRInput.GetUp(OVRInput.Button.PrimaryIndexTrigger, OVRInput.Controller.LTouch))
		{
			isFlying = false;
			//_rigidbody.useGravity = true;
			//print(_rigidbody.useGravity);
		}
		if (OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger, OVRInput.Controller.RTouch))
		{
			isFalling = true;
		}
		if (OVRInput.GetUp(OVRInput.Button.PrimaryIndexTrigger, OVRInput.Controller.RTouch))
		{
			isFalling = false;
		} 
	}
	
	private void FlyIfFlying()
	{
		if (isFlying)
		{
				Vector3 flyDirection = -(OculusPlayerFlightMode.instance.leftHand.transform.position - OculusPlayerFlightMode.instance.head.transform.position);
				transform.position += flyDirection.normalized * flyingSpeed;
		}
		
		/*
		if (isFalling)
		{
				Vector3 flyDirection = OculusPlayerFlightMode.instance.leftHand.transform.position - OculusPlayerFlightMode.instance.head.transform.position;
				transform.position += flyDirection.normalized * flyingSpeed;
		}
		*/
	}
	
	private void FreeFall()
	{
		if (flyingStarted)
		{
				Vector3 flyDirection = OculusPlayerFlightMode.instance.leftHand.transform.position - OculusPlayerFlightMode.instance.head.transform.position;
				transform.position += flyDirection.normalized * freefallSpeed;
		}
	}
	void FlightStickMovement()
	{
		Quaternion ort = CameraRig.centerEyeAnchor.rotation;
		Vector3 ortEuler = ort.eulerAngles;
		ortEuler.z = ortEuler.x = 0f;
		ort = Quaternion.Euler(ortEuler);
		Vector3 moveDir = Vector3.zero;
		
		
		Vector2 primaryAxis = OVRInput.Get(OVRInput.RawAxis2D.RThumbstick);
		//Vector2 primaryAxis = OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick);
		moveDir += ort * (primaryAxis.x * Vector3.right);
		moveDir += ort * (primaryAxis.y * Vector3.forward);
		//Debug.Log(OVRInput.Get(OVRInput.RawButton.LIndexTrigger));
		//_rigidbody.MovePosition(_rigidbody.transform.position + moveDir * FlightMovementSpeed * Time.fixedDeltaTime);
		gameObject.transform.position = gameObject.transform.position + moveDir * FlightMovementSpeed * Time.fixedDeltaTime;
		//gameObject.MovePosition(gameObject.position + moveDir * FlightMovementSpeed * Time.fixedDeltaTime);
	}
	
	void FlightSnapTurn()
	{
		if (OVRInput.Get(OVRInput.Button.PrimaryThumbstickLeft,OVRInput.Controller.LTouch))
		{
			if (ReadyToSnapTurn)
			{
				//ReadyToSnapTurn = false;
				transform.RotateAround(CameraRig.centerEyeAnchor.position, Vector3.up, -FlightRotationAngle);
			}
		}
		else if (OVRInput.Get(OVRInput.Button.PrimaryThumbstickRight,OVRInput.Controller.LTouch))
		{
			if (ReadyToSnapTurn)
			{
				//ReadyToSnapTurn = false;
				transform.RotateAround(CameraRig.centerEyeAnchor.position, Vector3.up, FlightRotationAngle);
			}
		}
		else
		{
			ReadyToSnapTurn = true;
		}
	}
	#endregion
}
