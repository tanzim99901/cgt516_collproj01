﻿using UnityEngine;
//
//For easily accesing local head and hand anchors
//
    public class OculusPlayerFlightMode : MonoBehaviour
    {
        public GameObject head;
        public GameObject rightHand;
        public GameObject leftHand;

        public static OculusPlayerFlightMode instance;

        private void Awake()
        {
            if (instance == null)
                instance = this;
        }

        private void OnDestroy()
        {
            if (instance == this)
                instance = null;
        }
    }
