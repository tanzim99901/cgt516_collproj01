﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Photon.Pun;
using Photon.Realtime;

public class NewCheckPhoton : MonoBehaviourPun
{
    // Start is called before the first frame update
    void Start()
    {
        if (photonView.IsMine == false)
		{
			for(int i = 0; i < transform.childCount; i++)
			{
				transform.GetChild(i).gameObject.SetActive(false);
			}
			GetComponent<OVRPlayerController>().enabled = false;
			GetComponent<CharacterController>().enabled = false;
			GetComponent<OVRSceneSampleController>().enabled = false;
			GetComponent<OVRDebugInfo>().enabled = false;
		}
		
		else if (photonView.IsMine == true)
		{
			for(int i = 0; i < transform.childCount; i++)
			{
				transform.GetChild(i).gameObject.SetActive(true);
			}
			GetComponent<OVRPlayerController>().enabled = true;
			GetComponent<CharacterController>().enabled = true;
			GetComponent<OVRSceneSampleController>().enabled = true;
			GetComponent<OVRDebugInfo>().enabled = true;
		}
    }

    // Update is called once per frame
    void Update()
    {
		Debug.Log(photonView.IsMine);
        /*if (photonView.IsMine == false)
		{
			for(int i = 0; i < transform.childCount; i++)
			{
				transform.GetChild(i).gameObject.SetActive(false);
			}
			GetComponent<OVRPlayerController>().enabled = false;
			GetComponent<CharacterController>().enabled = false;
			GetComponent<OVRSceneSampleController>().enabled = false;
			GetComponent<OVRDebugInfo>().enabled = false;
		}
		
		else if (photonView.IsMine == true)
		{
			for(int i = 0; i < transform.childCount; i++)
			{
				transform.GetChild(i).gameObject.SetActive(true);
			}
			GetComponent<OVRPlayerController>().enabled = true;
			GetComponent<CharacterController>().enabled = true;
			GetComponent<OVRSceneSampleController>().enabled = true;
			GetComponent<OVRDebugInfo>().enabled = true;
		}*/
    }
}

