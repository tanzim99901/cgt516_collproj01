﻿using System;
using System.Collections;


using UnityEngine;
using UnityEngine.SceneManagement;


using Photon.Pun;
using Photon.Realtime;


namespace Com.CGT516.ApplePicker
{
	
    public class GameManager : MonoBehaviourPunCallbacks
    {
		private string gameRoom = "GameRoom_01_Tanzim";
		#region public field
		
		public static GameManager Instance;
		
		
		[Tooltip("The prefab to use for representing the player")]
		public GameObject playerPrefab;
		
		#endregion
		
		
		#region Private Methods


		void LoadArena()
		{
			if (!PhotonNetwork.IsMasterClient)
			{
				Debug.LogError("PhotonNetwork : Trying to Load a level but we are not the master Client");
			}
			Debug.LogFormat("PhotonNetwork : Loading Level : GameRoom_01_Tanzim");
			PhotonNetwork.LoadLevel(gameRoom);
		}
		
		void Start()
		{
			Instance = this;
			
			if (playerPrefab == null)
			{
				Debug.LogError("<Color=Red><a>Missing</a></Color> playerPrefab Reference. Please set it up in GameObject 'Game Manager'",this);
			}
			else
			{
				if (NewPlayerManager.LocalPlayerInstance == null)
				{
					Debug.LogFormat("We are Instantiating LocalPlayer from {0}", SceneManagerHelper.ActiveSceneName);
					// we're in a room. spawn a character for the local player. it gets synced by using PhotonNetwork.Instantiate
					//spawnPoint[] sp = GameObject.FindObjectsOfType<spawnPoint> ();
					//int chosen = UnityEngine.Random.Range (0, sp.Length);
					//Vector3 position = sp [chosen].transform.position;
					PhotonNetwork.Instantiate(this.playerPrefab.name, new Vector3(0f,5f,0f), Quaternion.identity, 0);
					//PhotonNetwork.Instantiate(this.playerPrefab.name, position, Quaternion.identity, 0);
				}
				else{
					Debug.LogFormat("Ignoring scene load for {0}", SceneManagerHelper.ActiveSceneName);
					//PhotonNetwork.Instantiate(this.playerPrefab.name, new Vector3(5f,5f,0f), Quaternion.identity, 0);
					//PhotonNetwork.Instantiate(this.playerPrefab.name, position, Quaternion.identity, 0);
				}
			}
		}

		#endregion

        #region Photon Callbacks
		
		
		public override void OnPlayerEnteredRoom(Player other)
		{
			Debug.LogFormat("OnPlayerEnteredRoom() {0}", other.NickName); // not seen if you're the player connecting


			if (PhotonNetwork.IsMasterClient)
			{
				Debug.LogFormat("OnPlayerEnteredRoom IsMasterClient {0}", PhotonNetwork.IsMasterClient); // called before OnPlayerLeftRoom


				LoadArena();
			}
		}


		public override void OnPlayerLeftRoom(Player other)
		{
			Debug.LogFormat("OnPlayerLeftRoom() {0}", other.NickName); // seen when other disconnects


			if (PhotonNetwork.IsMasterClient)
			{
				Debug.LogFormat("OnPlayerLeftRoom IsMasterClient {0}", PhotonNetwork.IsMasterClient); // called before OnPlayerLeftRoom


				LoadArena();
			}
		}

        /// <summary>
        /// Called when the local player left the room. We need to load the launcher scene.
        /// </summary>
        public override void OnLeftRoom()
        {
            SceneManager.LoadScene(0);
        }


        #endregion


        #region Public Methods


        public void LeaveRoom()
        {
            PhotonNetwork.LeaveRoom();
        }


        #endregion
    }
}