﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Photon.Pun.UtilityScripts;

namespace Com.CGT516.ApplePicker
{
	public class FlightController : MonoBehaviourPun
	{
		//public GameObject head;
		//public GameObject leftHand;
		public float flyingSpeed;
		public float freefallSpeed;
		public float FlightMovementSpeed;
		public float FlightRotationAngle;
		public OVRCameraRig CameraRig;
		
		private bool isFlying = false;
		private bool isFalling = false;
		private bool ReadyToSnapTurn;
		private bool flyingStarted = false;
		// Start is called before the first frame update
		void Start()
		{
			
		}

		// Update is called once per frame
		void Update()
		{
			CheckIfFlying();
			FlyIfFlying();
			StickMovement();
			SnapTurn();
			FreeFall();
		}
		
		private void CheckIfFlying()
		{
			if (OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger, OVRInput.Controller.LTouch))
			{
				isFlying = true;
				flyingStarted = true;
			}
			if (OVRInput.GetUp(OVRInput.Button.PrimaryIndexTrigger, OVRInput.Controller.LTouch))
			{
				isFlying = false;
			}

			if (OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger, OVRInput.Controller.RTouch))
			{
				isFalling = true;
			}
			if (OVRInput.GetUp(OVRInput.Button.PrimaryIndexTrigger, OVRInput.Controller.RTouch))
			{
				isFalling = false;
			} 
		}
		
		private void FlyIfFlying()
		{
			if (isFlying)
			{
				Vector3 flyDirection = -(OculusPlayer.instance.leftHand.transform.position - OculusPlayer.instance.head.transform.position);
				transform.position += flyDirection.normalized * flyingSpeed;
			}
			
			if (isFalling)
			{
				Vector3 flyDirection = OculusPlayer.instance.leftHand.transform.position - OculusPlayer.instance.head.transform.position;
				transform.position += flyDirection.normalized * flyingSpeed;
			}
		}
		
		private void FreeFall()
		{
			if (flyingStarted)
			{
				Vector3 flyDirection = OculusPlayer.instance.leftHand.transform.position - OculusPlayer.instance.head.transform.position;
				transform.position += flyDirection.normalized * freefallSpeed;
			}
		}
		
		void StickMovement()
		{
			Quaternion ort = CameraRig.centerEyeAnchor.rotation;
			Vector3 ortEuler = ort.eulerAngles;
			ortEuler.z = ortEuler.x = 0f;
			ort = Quaternion.Euler(ortEuler);

			Vector3 moveDir = Vector3.zero;
			
			
			Vector2 primaryAxis = OVRInput.Get(OVRInput.RawAxis2D.RThumbstick);
			//Vector2 primaryAxis = OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick);
			moveDir += ort * (primaryAxis.x * Vector3.right);
			moveDir += ort * (primaryAxis.y * Vector3.forward);
			//Debug.Log(OVRInput.Get(OVRInput.RawButton.LIndexTrigger));
			//_rigidbody.MovePosition(_rigidbody.transform.position + moveDir * FlightMovementSpeed * Time.fixedDeltaTime);
			gameObject.transform.position = gameObject.transform.position + moveDir * FlightMovementSpeed * Time.fixedDeltaTime;
			//gameObject.MovePosition(gameObject.position + moveDir * FlightMovementSpeed * Time.fixedDeltaTime);
		}
		
		void SnapTurn()
		{
			if (OVRInput.Get(OVRInput.Button.PrimaryThumbstickLeft,OVRInput.Controller.LTouch))
			{
				if (ReadyToSnapTurn)
				{
					//ReadyToSnapTurn = false;
					transform.RotateAround(CameraRig.centerEyeAnchor.position, Vector3.up, -FlightRotationAngle);
				}
			}
			else if (OVRInput.Get(OVRInput.Button.PrimaryThumbstickRight,OVRInput.Controller.LTouch))
			{
				if (ReadyToSnapTurn)
				{
					//ReadyToSnapTurn = false;
					transform.RotateAround(CameraRig.centerEyeAnchor.position, Vector3.up, FlightRotationAngle);
				}
			}
			else
			{
				ReadyToSnapTurn = true;
			}
		}
	}
}
