﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class gateExit : MonoBehaviour
{
	public UnityEvent onCollide;
    // Start is called before the first frame update
    void Start()
    {
		
    }

    // Update is called once per frame
    void Update()
    {
        
    }
	
	
	private void OnTriggerEnter(Collider other)
	{
		//Debug.Log("Collided..........");
		if (other.tag == "human")
		{
			onCollide.Invoke();
		}
	}
}
