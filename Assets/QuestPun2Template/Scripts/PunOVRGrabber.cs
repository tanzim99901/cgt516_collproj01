﻿using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Com.CGT516.ApplePicker
{
    [RequireComponent (typeof (PhotonView))]
    public class PunOVRGrabber : OVRGrabber
    {
		// Controls Display
		public GameObject ControlsDisplay;
		public GameObject ControlsDisplayRake;
		public GameObject ControlsDisplayBox;
		public GameObject ControlsDisplayAssembly;
		
        
		PhotonView pv;

        //HandTracking related
        PunOVRHand trackingHand;
        private float pinchThreshold = 0.7f;

        protected override void Awake()
        {
            base.Awake();
            pv = GetComponent<PhotonView>();
            if(GetComponent<PunOVRHand>() != null)
            {
                trackingHand = GetComponent<PunOVRHand>();
            }
			ControlsDisplay.SetActive(false);
			ControlsDisplayRake.SetActive(false);
			ControlsDisplayBox.SetActive(false);
			ControlsDisplayAssembly.SetActive(false);

        }

        //Basically, if photonview is mine, update anchors from Grabber
        public override void Update()
        {
            if (pv.IsMine)
            {
                base.Update();
                //If hand tracking component detected, check pinch for grabbing mechanism.
                if(trackingHand != null)
                {
                    CheckPinch();
                }
				
				// For apple
				if (WhatGrabbed.tag == "apple")
				{
					ControlsDisplay.SetActive(true);
				}
				else
				{
					ControlsDisplay.SetActive(false);
				}
				
				// For apple
				if (WhatGrabbed.tag == "rake")
				{
					ControlsDisplayRake.SetActive(true);
				}
				else
				{
					ControlsDisplayRake.SetActive(false);
				}
				
				// For pick box
				if (WhatGrabbed.tag == "pick box")
				{
					ControlsDisplayBox.SetActive(true);
				}
				else
				{
					ControlsDisplayBox.SetActive(false);
				}
				
				// For Assembly components
				if (WhatGrabbed.tag == "Connection Collider")
				{
					ControlsDisplayAssembly.SetActive(true);
				}
				else
				{
					ControlsDisplayAssembly.SetActive(false);
				}
            }
        }

        //If the pinch strenght is bigger than the threshold, call GrabBegin(), if smaller, call GrabEnd().
        private void CheckPinch()
        {
            float pinchStrenght = trackingHand.GetFingerPinchStrength(PunOVRHand.HandFinger.Index);

            if(pinchStrenght > pinchThreshold)
            {
                GrabBegin();
                Debug.Log("Grab begin");
            }
            else if (pinchStrenght < pinchThreshold)
            {
                GrabEnd();
            }
        }
		
		
		
		
		
		
		// Collision overrides for turning controls display on for apples
		/*void OnTriggerEnter(Collider otherCollider)
		{
			//base.OnTriggerEnter(otherCollider);
			// Turn on Controls Display if apple is collided with
			if (otherCollider.tag == "apple")
			{
				ControlsDisplay.SetActive(true);
			}
			
			else
			{
				ControlsDisplay.SetActive(false);
			}
		}
		
		void OnTriggerExit(Collider otherCollider)
		{
			//base.OnTriggerExit(otherCollider);
			// Turn off Controls Display if apple
			ControlsDisplay.SetActive(false);
		}*/
    }
}
