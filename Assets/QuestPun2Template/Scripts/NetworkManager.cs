﻿using UnityEngine;
using Photon.Realtime;
using Photon.Pun;
using UnityEngine.SceneManagement;
using System.Collections;
using UnityEngine.UI;
using TMPro;

//
//This script connects to PHOTON servers and creates a room if there is none, then automatically joins
//
namespace Com.CGT516.ApplePicker
{
    public class NetworkManager : MonoBehaviourPunCallbacks
    {
		/// <summary>
		/// The maximum number of players per room. When a room is full, it can't be joined by new players, and so new room will be created.
		/// </summary>
		[Tooltip("The maximum number of players per room. When a room is full, it can't be joined by new players, and so new room will be created")]
		[SerializeField]
		private byte maxPlayersPerRoom = 4;
		
		[Tooltip("The name of the room to create or join")]
		[SerializeField]
		private string _roomName;
		
		/// <summary>
        /// This client's version number. Users are separated from each other by gameVersion (which allows you to make breaking changes).
        /// </summary>
        string gameVersion = "v1";
		/// <summary>
		/// Keep track of the current process. Since connection is asynchronous and is based on several callbacks from Photon,
		/// we need to keep track of this to properly adjust the behavior when we receive call back by Photon.
		/// Typically this is used for the OnConnectedToMaster() callback.
		/// </summary>
		bool isConnecting;
		
        bool triesToConnectToMaster = false;
        bool triesToConnectToRoom = false;
		
		bool enterPressed = false;
		
		
		// <summary>
        /// MonoBehaviour method called on GameObject by Unity during initialization phase.
        /// </summary>
        void Start()
        {
            //Connect();

			
			//Connect();
        }
		
		
		
        private void Update()
        {
            if (!PhotonNetwork.IsConnected && !triesToConnectToMaster)
            {
                ConnectToMaster();
            }
            if (PhotonNetwork.IsConnected && !triesToConnectToMaster && !triesToConnectToRoom)
            {
                StartCoroutine(WaitFrameAndConnect());
            }
        }
		
		public void loginSwitch()
		{
			enterPressed = true;
		}
		
		/*public void getPlayerName()
		{
			playerName = nameInput.GetComponent<TMP_InputField>().text;;
			//Debug.Log(playerName);
		}*/
		
        public void ConnectToMaster()
        {
            PhotonNetwork.OfflineMode = false; //true would "fake" an online connection
			//getPlayerName();
            PhotonNetwork.NickName = "name"; //we can use a input to change this 
            PhotonNetwork.AutomaticallySyncScene = true; //To call PhotonNetwork.LoadLevel()
            PhotonNetwork.GameVersion = gameVersion; //only people with the same game version can play together

            triesToConnectToMaster = true;
            //PhotonNetwork.ConnectToMaster(ip, port, appid); //manual connection
            PhotonNetwork.ConnectUsingSettings(); //automatic connection based on the config file
        }

        public override void OnDisconnected(DisconnectCause cause)
        {
            base.OnDisconnected(cause);
            triesToConnectToMaster = false;
            triesToConnectToRoom = false;
            Debug.Log(cause);
        }

        public override void OnConnectedToMaster()
        {
            base.OnConnectedToMaster();
            triesToConnectToMaster = false;
            Debug.Log("Connected to master!");
        }

        IEnumerator WaitFrameAndConnect()
        {
            triesToConnectToRoom = true;
            yield return new WaitForEndOfFrame();
            Debug.Log("Connecting");
            ConnectToRoom();
        }

        public void ConnectToRoom()
        {
            if (!PhotonNetwork.IsConnected)
                return;

            triesToConnectToRoom = true;
			RoomOptions options = new RoomOptions();
			options.MaxPlayers = maxPlayersPerRoom;
			PhotonNetwork.JoinOrCreateRoom(_roomName, options, TypedLobby.Default);
            //PhotonNetwork.JoinOrCreateRoom(_roomName); //Create a specific room - Callback OnCreateRoomFailed
            //PhotonNetwork.JoinRoom(_roomName); //Join a specific room - Callback OnJoinRoomFailed

            //PhotonNetwork.JoinRandomRoom(); // Join a random room - Callback OnJoinRandomRoomFailed
        }

        public override void OnJoinedRoom()
        {
            //Go to next scene after joining the room
            base.OnJoinedRoom();
            Debug.Log("Master: " + PhotonNetwork.IsMasterClient + " | Players In Room: " + PhotonNetwork.CurrentRoom.PlayerCount + " | RoomName: " + PhotonNetwork.CurrentRoom.Name + " Region: " + PhotonNetwork.CloudRegion);
            SceneManager.LoadScene(1); //go to the room scene
        }

        public override void OnJoinRandomFailed(short returnCode, string message)
        {
            base.OnJoinRandomFailed(returnCode, message);
            //no room available
            //create a room (null as a name means "does not matter")
            PhotonNetwork.CreateRoom(_roomName, new RoomOptions { MaxPlayers = maxPlayersPerRoom });
        }
    }
}