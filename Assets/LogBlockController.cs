﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Com.CGT516.ApplePicker
{
	public class LogBlockController : MonoBehaviour
	{
		private Rigidbody rb;
		private PunOVRGrabbable grab01;
		
		public GameObject AllColliders;
		public int GrabbableLayer;
		public int DefaultLayer;

		void Start()
		{
			rb = GetComponent<Rigidbody>();
		}

		void Update()
		{
			grab01 = GetComponent<PunOVRGrabbable>();
			
			if (grab01.isGrabbed)
			{
				AllColliders.layer = GrabbableLayer;
			}
			else
			{
				AllColliders.layer = DefaultLayer;
			}
		}
	}
}
