﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using Photon.Realtime;
using Photon.Pun;

public class weedRemover : MonoBehaviour
{
	//public AudioClip cuttingSound;
	//private AudioSource cutAudio;
	//public Text collideInfo;
    // Start is called before the first frame update
    void Start()
    {
        //collideInfo.text = "Z";
    }

    // Update is called once per frame
    void Update()
    {
        
    }
	
	private void OnTriggerEnter(Collider other)
	{
		if (other.tag == "rake")
		{
			//cutAudio = GetComponent<AudioSource>();
			//cutAudio.PlayOneShot(cuttingSound, 2.0f);
			//gameObject.SetActive(false);
			//Destroy(gameObject);
			//collideInfo.text = "Rake collided";
			GetComponent<PhotonView>().RPC("RPC_DestroyObject", RpcTarget.AllBuffered, 1);
		}
	}
	
	[PunRPC]
	void RPC_DestroyObject(int n)
	{
		Destroy(gameObject);
	}
		
}
