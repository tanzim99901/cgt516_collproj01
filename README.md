This is a collaborative virtual environment, where multiple users can participate in simple apple picking and gardening tasks in a garden. 
Implemented in Unity 2019.4.29f1 using Photon 2, with OculusVR support.
This current version includes three different scenes: A lobby and a day version and a night version of the garden.

Lobby scene: Assets/Scenes/Levels/Main_Lobby.unity
Day scene:  Assets/Scenes/Levels/Garden_Day.unity
Night scene:  Assets/Scenes/Levels/Garden_Night.unity

A build is included in: Builds/Day&NightScene/

You will need an Oculus Rift or an Oculus Quest (with Oculus Link) to run the build.

We are currently working on adding some collaborative assembly tasks where multiple users will be able to collaborate together to assemble different objects.

Future plans include creating a large-scale virtual world, with various terrain and weather conditions synced with the real world.

Demo: https://youtu.be/MCFGCUhp5ng